<?php

namespace Drupal\commerce_tax_covid_de\EventSubscriber;

use Drupal\commerce_tax\TaxZone;
use Drupal\commerce_tax_covid\Event\TaxCovidEvents;
use Drupal\commerce_tax_covid\Event\TaxCovidZonesEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class GermanyReducedTax
 *
 * @package Drupal\commerce_tax_covid_de\EventSubscriber
 */
class GermanyReducedTax implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      TaxCovidEvents::COMMERCE_TAX_COVID_ZONES => ['alterZones'],
    ];
    return $events;
  }

  /**
   * React on zones.
   *
   * @param \Drupal\commerce_tax_covid\Event\TaxCovidZonesEvent $event
   *   The tax zones event.
   */
  public function alterZones(TaxCovidZonesEvent $event) {
    $zones = $event->getZones();

    // Copy entire zone for now, until we add in core
    // altering definitions prior creating TaxZone.
    // @see https://www.drupal.org/project/commerce/issues/3145804#comment-13681808
    if (isset($zones['de'])) {
      $zones['de'] = new TaxZone([
        'id' => 'de',
        'label' => $this->t('Germany'),
        'display_label' => $this->t('VAT'),
        'territories' => [
          // Germany without Heligoland and Büsingen.
          ['country_code' => 'DE', 'excluded_postal_codes' => '27498, 78266'],
          // Austria (Jungholz and Mittelberg).
          ['country_code' => 'AT', 'included_postal_codes' => '6691, 6991:6993'],
        ],
        'rates' => [
          [
            'id' => 'standard',
            'label' => $this->t('Standard'),
            'percentages' => [
              ['number' => '0.19', 'start_date' => '2007-01-01'],
              ['number' => '0.16', 'start_date' => '2020-07-01', 'end_date' => '2020-12-31'],
            ],
            'default' => TRUE,
          ],
          [
            'id' => 'reduced',
            'label' => $this->t('Reduced'),
            'percentages' => [
              ['number' => '0.07', 'start_date' => '1983-07-01'],
              ['number' => '0.05', 'start_date' => '2020-07-01', 'end_date' => '2020-12-31']
            ],
          ],
        ],
      ]);

      $event->setZones($zones);
    }

  }
}
