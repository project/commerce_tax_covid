<?php

namespace Drupal\commerce_tax_covid\Plugin\Commerce\TaxType;

use Drupal\commerce_tax\Plugin\Commerce\TaxType\CanadianSalesTax;

/**
 * Class CovidCanadianSalesTax
 *
 * @package Drupal\commerce_tax_covid\Plugin\Commerce\TaxType
 */
class CovidCanadianSalesTax extends CanadianSalesTax {

  use Covid19Trait;
}
