<?php

namespace Drupal\commerce_tax_covid\Plugin\Commerce\TaxType;

use Drupal\commerce_tax\Plugin\Commerce\TaxType\EuropeanUnionVat;

/**
 * Class CovidEuropeanUnionVat
 *
 * @package Drupal\commerce_tax_covid\Plugin\Commerce\TaxType
 */
class CovidEuropeanUnionVat extends EuropeanUnionVat {

  use Covid19Trait;
}
