<?php

namespace Drupal\commerce_tax_covid\Plugin\Commerce\TaxType;

use Drupal\commerce_tax\Plugin\Commerce\TaxType\SwissVat;

/**
 * Class CovidSwissVat
 *
 * @package Drupal\commerce_tax_covid\Plugin\Commerce\TaxType
 */
class CovidSwissVat extends SwissVat {

  use Covid19Trait;
}
