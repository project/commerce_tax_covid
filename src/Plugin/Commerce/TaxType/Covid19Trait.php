<?php

namespace Drupal\commerce_tax_covid\Plugin\Commerce\TaxType;

use Drupal\commerce_tax_covid\Event\TaxCovidEvents;
use Drupal\commerce_tax_covid\Event\TaxCovidZonesEvent;

/**
 * Trait Covid19Trait
 *
 * @package Drupal\commerce_tax_covid\Plugin\Commerce\TaxType
 */
trait Covid19Trait {

  /**
   * {@inheritdoc}
   */
  public function getZones() {
    if (empty($this->zones)) {
      $this->zones = $this->buildZones();
      $event = new TaxCovidZonesEvent($this->zones);
      $this->zones = $this->eventDispatcher->dispatch(TaxCovidEvents::COMMERCE_TAX_COVID_ZONES, $event);
    }

    return $this->zones;
  }

}
