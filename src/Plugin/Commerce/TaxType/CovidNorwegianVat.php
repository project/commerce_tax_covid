<?php

namespace Drupal\commerce_tax_covid\Plugin\Commerce\TaxType;

use Drupal\commerce_tax\Plugin\Commerce\TaxType\NorwegianVat;

/**
 * Class CovidNorwegianVat
 *
 * @package Drupal\commerce_tax_covid\Plugin\Commerce\TaxType
 */
class CovidNorwegianVat extends NorwegianVat {

  use Covid19Trait;
}
