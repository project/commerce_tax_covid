<?php

namespace Drupal\commerce_tax_covid\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the Covid 19 event.
 *
 * @see \Drupal\commerce_tax_covid\Event\TaxEvents
 */
class TaxCovidZonesEvent extends Event {

  /**
   * The zones.
   *
   * @var array
   */
  protected $zones;

  /**
   * Constructs a new TaxCovidZonesEvent.
   *
   * @param $zones
   */
  public function __construct($zones) {
    $this->zones = $zones;
  }

  /**
   * Gets the customer profile.
   *
   * @return array|null
   *   The zones, or NULL if not yet known.
   */
  public function getZones() {
    return $this->zones;
  }

  /**
   * Sets the zones.
   *
   * @param \Drupal\commerce_tax\TaxZone[] $zones
   *
   * @return $this
   */
  public function setZones(array $zones) {
    $this->zones = $zones;
    return $this;
  }

}
