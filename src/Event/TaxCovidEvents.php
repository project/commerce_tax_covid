<?php

namespace Drupal\commerce_tax_covid\Event;

/**
 * Class TaxCovidEvents
 *
 * @package Drupal\commerce_tax_covid\Event
 */
final class TaxCovidEvents {

  /**
   * Name of the event fired when listing all zones.
   *
   * @Event
   *
   * @see \Drupal\commerce_tax_covid\Event\TaxCovidZonesEvent
   */
  const COMMERCE_TAX_COVID_ZONES = 'commerce_tax_covid.zones';

}
